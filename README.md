# DB Schema

We use [DBML](https://www.dbml.org/home/) for sketching out schema changes and generating documentation.

We use [dbmate](https://github.com/amacneil/dbmate) for generating and managing the schema migrations.

Reading through the docs for both will help with context for using this repository.

## Getting started

* Install dbmate using the appropriate instructions for your OS from the link above.
* Run the migrations via dbmate i.e. `$ dbmate --url 'postgres://postgres@localhost:5432/upchieve?sslmode=disable' up`
  * Alternatively, directly apply the schema.sql file i.e. `$ psql -d upchieve -f ./db/schema.sql
* Follow instructions for seeding in Subway rpeo

## Making changes

When updating the schema, you'll need to run `$ dbmate up`

## Notes

### Why composite primary keys for join tables?

https://weblogs.sqlteam.com/jeffs/2007/08/23/composite_primary_keys/

### What is a ULID?

We're using ULIDs because they are similar to UUIDs in randomness, but their initial bytes are generated from a timestamp, so they are far friendlier for database indexing.

* [The ULID Spec](https://github.com/ulid/spec)
* [JS/TS implementation](https://github.com/aarondcohen/id128#ulid)
* [Ruby implementation](https://github.com/rafaelsales/ulid)
